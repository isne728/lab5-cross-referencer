#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "tree.h"
using namespace std;

void main()
{
	Tree<string> mytree;
    string line, word;
    ifstream myfile("song.txt"); //input file
    char count = '1';
    
    if (myfile.is_open())
    {
        while (getline(myfile, line)) //keep myfile in line
        {
            istringstream eachline(line);
            while (getline(eachline, word, ' ')) { //keep eachline in word
                word = word + count; //keep word and line number
                mytree.insert(word);
            }
            count++; //increase line number
        }
        myfile.close();
    }

    else cout << "Unable to open file"; //print this sentence when it can't find file

    mytree.inorder();
}

